This is the supervisor agreement for our group in the p2 project. This file will serve as an outline for how we will work with our supervisor.

1. How often can we expect a supervisor meeting?
    1. Weekly meetings, with the possibility of multiple meetings if necessary. The supervisor is very flexible, as long as we schedule well in advance
    2. Weekly/Biweekly
2. When are we to plan next meeting?
    1. We’ll discuss the date, time and place of the next meeting at the end any meeting, or by email, if anything comes up. Scheduling of new meetings has to be done as soon as possible, preferably during or right after the current meeting.
3. What are the expectations in relation to meeting agenda, summaries and moderation?
   1. The supervisor is very pleased with how we want to structure the meetings. Agenda is mostly controlled by us, we write up a minute of the meeting, which we’ll send to the supervisor before the next meeting along with any other relevant material.
4. What material does the supervisor prefer to receive as a prerequisite for the meeting?
   1. We are allowed to send almost everything. We have agreed to grant the supervisor access to our Overleaf document, so that we don’t have to send a new copy at every meeting.
5. How many reading instructions would the supervisor prefer to receive?
   1. The supervisor does not expect anything, but any info, to help guide what to read and what to have in mind while reading is very much welcome.
6. How well in advance would the supervisor like to receive written material, leaving enough time to read it as well?
   1. 1 or 2 days prior to the meeting, at least.
7. Does the supervisor prefer some file formats over others?
   1. No. If, for some reason or another, the supervisor can’t open a file, he’ll get in contact with us.