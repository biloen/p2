This is the group contract for our group in the p2 project. This file will serve as an outline for how our work will be conducted.  
It will also contain a styleguide for our work.  

1. The group will meet as often as possible in our assigned group room, and work from there.
    1. However, if it is otherwise agreed upon, work will happen from home.
    2. During lockdown in the pandemic, work will be conducted from our discord group.
2. If a group member is sick or delayed more than 20 min, they will notify the group as soon as possible.
3. We will take our work seriously:
    1. Proper formatting in our code.
    2. Set daily goals for ourselves and the group as a whole.
    3. The group will prepare for every meeting with our supervisor.
4. Refrences:
    1. Remember to add a refrence, whenever you read something new.
    2. Whenever you add a refrence to Zotero, make sure to verify information, and add missing points (authors, access date etc.).
    3. Please don't plagiarize.
5. The report:
    1. Everything you write must be read through by two people in the group, who didn't participate in writing that paragraph.
    2. Bigger changes to a section that you didn't write yourself must be notified to the entire group.
6. The program:
    1. The program will be written in C, and possibly javascript, and all guidelines must be followed:
        1. Proper comments.
        2. Proper variable names (snake_case).
        3. Proper function names (PascalCase).
        4. Proper naming of structures and data types (PascalCase).
        5. Readable layout.
        6. 4 space indentation.
        7. Avoid global state.
    2. All group members have a say in the program, and how it will end up.
    3. The program source will be controlled through Git.
7. Git conduct:
    1. Avoid comitting directly to master.
    2. Git messages should be serious and should descibe the contents properly.
8. Group culture:
    1. We have to put enough work into the project such that everyone ends up satisfied with the end result.
    2. In case of conflict within the group we should attempt to resolve it through communication.
    3. Before starting work on part of a project it should be communicated to other members to ensure that the work is properly coordinated.
9. The ILS distribution of our group:
    1. In our group we were wildly spread out on all the 4 parameters. This means that we might have some problems working together, as we will have different ways we prefer to learn.
    2. This means that in our group we have to let the other groupmembers work in their own way, to avoid conflicts.  
    3. However, there are also advantages to our differences. Because of our wide distribution of learning styles,      
       we should be able to complement each other’s weaknesses. 
    4. Overall, our group is facing multiple obstacles and opportunities due to our diversity, but we will do  
       our best to make use of the opportunities to overcome the obstacles.  
