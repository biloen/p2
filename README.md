# P2 - A Blockchain Voting System for Digital Elections
This programme has been developed by the data science group, B2-27, at Aalborg University.

## About this Project
We have set out to develop a digital internet voting system to pave the way for future elections in Denmark, allowing voters to hand in their voting ballot over the internet, through a smart device or computer. It is our hope that it will help to increase the voter turnout and make it cheaper to hold elections. A digital voting system would come with a lot of advantages; but also some disadvantages, we would have to iron out to move towards a succesful implementation.

### Build / Compile Instructions

The project should be available to be accessed from https://walsted.dev:8443/ as long as the servers are online.

[Currently]

First step is to clone the repository

`git clone https://gitlab.com/biloen/p2`

Then go into to the project folder

`cd p2`

To download the nessecary packages run

`npm install`

Then start the backend server using

`node src/backend/server.js`

And lastly start the blockchain api using

`node  src/backend/node/api.js`

Then, in your webbrowser, navigate to http://127.0.0.0:3050

The blockchain api will be running on http://127.0.0.0:3051

### Unit testing Instructions

First step is to install development dependencies.

`npm install --production=false`

The final step is to run the debugger.

`npm test`

### API endpoints
* get/block
  * Parameters: {hash: string}
* get/blocks/date
  * Parameters: {start: unix_time, end: unix_time}
* get/blocks/height
  * Parameters: {start: integer, end: integer}
* get/latest/block
  * Parameters: {None}
* get/latest/blocks
  * Parameters: {limit: integer}
* get/transaction
  * Parameters: {hash: string}
* get/unconfirmed/transaction
  * Parameters: {hash: string}
* get/transactions
  * Parameters: {block_hash: string}
* get/latest/transaction
  * Parameters: {None}
* new/transaction
  * Parameters: {sender: string, vote: string, signature: string, timestamp: unix_time, hash: string}
* get/results
  * Parameters: {None}

### Who is on the Team?
Christoffer Trebbien Jønsson - cjanss20@student.aau.dk\
Frederik Flemming Kærup Rasmussen - ffkr20@student.aau.dk\
Jamie Lee Smith Hammer - jhamme20@student.aau.dk\
Loke Walsted - lwalst20@student.aau.dk\
Mads Lundfold - mlundf20@student.aau.dk\
Marcus Thorbøll Jespersgaard - mjesp20@student.aau.dk\
Rasmus Hooge - rhooge20@student.aau.dk

### Project Layout
Flowchart of the programme can be found here: 



### Written Report
We have chosen to write and format our report in LaTeX with Overleaf. A link can be found here: https://www.overleaf.com/read/rqghskhbxgwd
